package com.ernok.ima;

public class ImaFileRequest implements ImaRequest {

	public String path;
	
	public ImaFileRequest(String path) {
		this.path = path;
	}
	
	@Override
	public Type getType() {
		return Type.File;
	}

}
