package com.ernok.ima;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/*
 * Static methods for using IMA protocol.
 */
public class ImaProtocol {

	static final int MAX_MESSAGE_SIZE = 1024 * 1024;

	/*
	 * Receive an instance of ImaRequest from a client
	 */
	public static ImaRequest listenRequest(Socket client) throws IOException {
		byte[] buffer = new byte[MAX_MESSAGE_SIZE];
		int len;

		// receive request
		len = client.getInputStream().read(buffer);
		if (len < 0) {
			// client disconnected
			return null;
		}

		// check request type
		switch (buffer[0]) {

			case 'F': {
				String file = new String(buffer, 1, len - 1);
				return new ImaFileRequest(file);
			}

			default: {
				throw new RuntimeException("Client request error");
			}

		}
	}

	/*
	 * Send 'E' + errorMsg
	 */
	public static void sendError(Socket peer, String errorMsg) throws IOException {
		byte[] buffer = new byte[MAX_MESSAGE_SIZE];

		buffer[0] = 'E';

		byte[] errorBytes = errorMsg.getBytes();
		for (int i = 0; i < errorBytes.length; ++i) {
			buffer[1 + i] = errorBytes[i];
		}

		peer.getOutputStream().write(buffer, 0, 1 + errorBytes.length);
	}

	/*
	 * Send 'O' + file
	 */
	public static void sendFile(Socket peer, String file) throws IOException {
		byte[] buffer = new byte[MAX_MESSAGE_SIZE];

		buffer[0] = 'O';

		byte[] fileBytes = file.getBytes();
		for (int i = 0; i < fileBytes.length; ++i) {
			buffer[1 + i] = fileBytes[i];
		}

		peer.getOutputStream().write(buffer, 0, 1 + fileBytes.length);
	}

	/*
	 * Pedirle al server en addr:port un archivo. Retorna el contenido del archivo,
	 * o null si no existe.
	 * 
	 * Formato de peticion de archivo:
	 * 
	 * - 1 byte: ascii 'F'
	 * 
	 * - N bytes: path al archivo; ex: "/index.html"
	 * 
	 */
	public static String requestFile(String host, int port, String path) throws UnknownHostException, IOException {
		byte[] buffer = new byte[MAX_MESSAGE_SIZE];
		int answerLen;

		try (Socket server = new Socket(host, port);) {
			// send request
			{
				byte[] pathBytes = path.getBytes();
				buffer[0] = 'F';
				for (int i = 0; i < pathBytes.length; ++i) {
					buffer[1 + i] = pathBytes[i];
				}
				server.getOutputStream().write(buffer, 0, 1 + pathBytes.length);
			}

			// receive response
			{
				answerLen = server.getInputStream().read(buffer);
				if (answerLen < 0) {
					throw new RuntimeException("Server error");
				}
			}

			// check answer
			switch (buffer[0]) {

				// Error, throw exception with error message
				case 'E': {
					String errorMsg = new String(buffer, 1, answerLen - 1);
					throw new RuntimeException(String.format("Error: %s", errorMsg));
				}

				// Ok, return file
				case 'O': {
					String file = new String(buffer, 1, answerLen - 1);
					return file;
				}

				// protocol error
				default: {
					throw new RuntimeException("Server reponse error");
				}

			}
		}
	}
}
