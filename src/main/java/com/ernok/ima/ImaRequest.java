package com.ernok.ima;

public interface ImaRequest {
	
	public static enum Type {
		File,
	}
	
	Type getType();
	
}
